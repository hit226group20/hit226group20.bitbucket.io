const form = document.getElementById('form');
const username = document.getElementById('username');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');
const email = document.getElementById('email');
const address = document.getElementById('address');
const age = ( document.getElementById('age'));

form.addEventListener('submit', (e) => {
    e.preventDefault();

    checkInputs();
})

function checkInputs(){
    //values from inputs
    const usernameValue = username.value.trim();
    const passwordValue = password.value.trim();
    const password2Value = password2.value.trim();
    const emailValue = email.value.trim();
    const addressValue = address.value.trim();
    const ageValue = age.value.trim();
    

    if (usernameValue === ''){
        //shows error
        setErrorFor(username, "Please enter Username")
    } else {
        setSuccessFor(username, "You have successfully entered your username");
        
    }

    if (emailValue === ""){
        setErrorFor(email, "Email cannot be blank");
    } else if (!isEmail(emailValue)) {
        setErrorFor(email, "Email is not Valid")
    } else {
        setSuccessFor(email, "You have succesfully entered your email");
    }

    if(passwordValue === ""){
        
        //shows error
        setErrorFor(password, "Please enter Password")
    } else {
        setSuccessFor(password, "You have successfully entered your password");
        
    }

    if(password2Value === ""){
        
        //shows error
        setErrorFor(password2, "Please enter Password")
    } else if (passwordValue !== password2Value) {
        setErrorFor(password2, "Please enter the same Password");

    } else {
        setSuccessFor (password2, "Looks Great!");
    } 

    if (addressValue === ''){
        //shows error
        setErrorFor(address, "Please enter Address")
    } else {
        setSuccessFor(address, "You have successfully entered your Address");
        
    }

    if (ageValue === ''){
            setErrorFor(age, "Please enter Age");
        
    } else {
        setSuccessFor(age, "You have successfully entered your Age");
        

    } 


    }

    



function setErrorFor(input, message){
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');

    
    small.innerText = message;

    formControl.className = 'form-control error';
}

function setSuccessFor(input, message){
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');

    
    small.innerText = message;

    formControl.className = 'form-control success';
}
function isEmail(email){
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);

}

